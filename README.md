# Power Automate
My daily routine, aided by Power Automate

**Here is the purpose of each script:**

- **Work:** Workflows and routines used at my job
    - **Share My Meeting Status:**  Send a notification to Pushover whenever a meeting is about to start and light the indicator LED on my desk